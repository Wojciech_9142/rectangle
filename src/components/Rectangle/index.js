import React, { useEffect, useState } from 'react';

const RectangleInfo = (props) => {
    const [obj, setObj] = useState(0);

    useEffect(() => {
        setTimeout(()=>{
        const elWidth = props.size.current.clientWidth;
        const elHeight = props.size.current.clientHeight;
        setObj(() => elWidth * elHeight);
        }, 500)
    }, [props]);
    return (
        <div id="element">
            <p>Objętość prostokąta równa jest {obj} px<sup>2</sup></p>
        </div>
    )
}

export default RectangleInfo;