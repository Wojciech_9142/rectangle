import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import './App.css';
import RectangleInfo from './components/Rectangle';

const Rectangle = styled.div`
  width: ${props => props.width + 'px'};
  height: ${props => props.height + 'px'};
  background-color: red;
  transition: .4s linear;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-size: 18px;
`;

function App() {
  const [sizes, setSizes] = useState({ "width": 100, "height": 100 });
  const size = useRef();
  const min = 100;
  const max = 500;

  const changeSize = () => {
    let width = Math.floor(Math.random() * (max - min)) + min;
    let height = Math.floor(Math.random() * (max - min)) + min;
    setSizes({ "width": width, "height": height });
    console.log(`new width: ${width} and new height: ${height}`);
  }

  return (
    <div className="App">
      <Rectangle ref={size} width={sizes.width} height={sizes.height}>
        <RectangleInfo {...{ size }} />
      </Rectangle>
      <div>
        <p>Szerokość to : {sizes.width}</p>
        <p>Wysokość to : {sizes.height}</p>
      </div>
      <button onClick={changeSize}>Wylosuj wymiary</button>
    </div>
  );
}

export default App;
